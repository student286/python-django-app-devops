terraform {
  backend "s3" {
    bucket         = "recipe-jan-bucket"
    key            = "recipe-app.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "recipe-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-west-1"
  version = "~> 4.67.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}


data "aws_region" "current" {

}

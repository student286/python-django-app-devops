variable "prefix" {
  default = "recipe"
}

variable "project" {
  default = "terraform-gitlab-cicd"
}

variable "contact" {
  default = "prashant@mail.com"
}

variable "db_username" {
  description = "sername for rds postgres instance"
}

variable "db_password" {
  description = "password for rds postgress instance"
}

variable "bastion_key_name" {
  default = "recipe"
}

variable "ecr_image_api" {
  description = "ecr image for api"
  default     = "058264318202.dkr.ecr.us-west-1.amazonaws.com/recipe-app:latest"
}

variable "ecr_image_proxy" {
  description = "ecr image for proxy"
  default     = "058264318202.dkr.ecr.us-west-1.amazonaws.com/nginx-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for django app"
}

variable "dns_zone_name" {
  description = "domain name"
  default     = "helloprashant.xyz"
}

variable "subdomain" {
  description = "sub-domain per environment"
  type        = map(string)
  default = {
    "production" = "api"
    "staging"    = "api.stage"
    "dev"        = "api.dev"
  }
}

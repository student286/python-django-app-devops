resource "aws_vpc" "main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-vpc")
  )
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-igw")
  )
}


resource "aws_subnet" "public_subnet_a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pub-sub-a")
  )

}

resource "aws_route_table" "public_rtb_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pub-rtb-a")
  )

}

resource "aws_route_table_association" "public_rtb_assoc_a" {
  subnet_id      = aws_subnet.public_subnet_a.id
  route_table_id = aws_route_table.public_rtb_a.id
}


resource "aws_route" "public_internet_access_a" {
  route_table_id         = aws_route_table.public_rtb_a.id
  gateway_id             = aws_internet_gateway.igw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_eip" "eip_1" {
  vpc = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-eip-1")
  )

}

resource "aws_nat_gateway" "nat_1" {
  allocation_id = aws_eip.eip_1.id
  subnet_id     = aws_subnet.public_subnet_a.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-natgw-1")
  )

}


#################################################################################



resource "aws_subnet" "public_subnet_b" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pub-sub-b")
  )

}

resource "aws_route_table" "public_rtb_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pub-rtb-b")
  )

}

resource "aws_route_table_association" "public_rtb_assoc_b" {
  subnet_id      = aws_subnet.public_subnet_b.id
  route_table_id = aws_route_table.public_rtb_b.id
}


resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_rtb_b.id
  gateway_id             = aws_internet_gateway.igw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_eip" "eip_2" {
  vpc = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-eip-2")
  )

}

resource "aws_nat_gateway" "nat_2" {
  allocation_id = aws_eip.eip_2.id
  subnet_id     = aws_subnet.public_subnet_b.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-natgw-2")
  )

}


#################################################################################


resource "aws_subnet" "private_subnet_a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.1.10.0/24"
  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pvt-sub-a")
  )

}

resource "aws_route_table" "private_rtb_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pvt-rtb-a")
  )

}

resource "aws_route_table_association" "private_rtb_assoc_a" {
  subnet_id      = aws_subnet.private_subnet_a.id
  route_table_id = aws_route_table.private_rtb_a.id

}

resource "aws_route" "private_internet_access_a" {
  route_table_id         = aws_route_table.private_rtb_a.id
  nat_gateway_id         = aws_nat_gateway.nat_1.id
  destination_cidr_block = "0.0.0.0/0"
}



#################################################################################


resource "aws_subnet" "private_subnet_b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.1.11.0/24"
  availability_zone = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pvt-sub-b")
  )

}

resource "aws_route_table" "private_rtb_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-pvt-rtb-b")
  )

}

resource "aws_route_table_association" "private_rtb_assoc_b" {
  subnet_id      = aws_subnet.private_subnet_b.id
  route_table_id = aws_route_table.private_rtb_b.id

}

resource "aws_route" "private_internet_access_b" {
  route_table_id         = aws_route_table.private_rtb_b.id
  nat_gateway_id         = aws_nat_gateway.nat_2.id
  destination_cidr_block = "0.0.0.0/0"
}



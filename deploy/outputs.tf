output "db_host" {
  value = aws_db_instance.database.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_route53_record.zone_record.fqdn
}
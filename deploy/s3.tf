resource "aws_s3_bucket" "recipe_s3_bucket" {
  bucket        = "${local.prefix}-recipe-jan-files"
  force_destroy = true
}

resource "aws_s3_bucket_ownership_controls" "recipe_s3_bucket_acl" {
  bucket = aws_s3_bucket.recipe_s3_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "recipe_s3_public_acces_block" {
  bucket = aws_s3_bucket.recipe_s3_bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false

}
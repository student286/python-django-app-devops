resource "aws_ecs_cluster" "recipe_ecs_cluster" {
  name = "${local.prefix}-cluster"
  tags = local.common_tags

}

resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")

}

resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags

}


resource "aws_iam_role_policy_attachment" "task_execution_role_attach" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn

}


resource "aws_iam_role" "recipe_app_iam_role" {
  name               = "${local.prefix}-recipe-task-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags

}

resource "aws_cloudwatch_log_group" "api_log_group" {
  name = "${local.prefix}-api-log-group"

  tags = local.common_tags

}


data "template_file" "api_container_definitions" {

  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image                = var.ecr_image_api
    proxy_image              = var.ecr_image_proxy
    django_secret_key        = var.django_secret_key
    db_host                  = aws_db_instance.database.address
    db_name                  = aws_db_instance.database.name
    db_user                  = aws_db_instance.database.username
    db_pass                  = aws_db_instance.database.password
    log_group_name           = aws_cloudwatch_log_group.api_log_group.name
    log_group_region         = data.aws_region.current.name
    allowed_hosts            = aws_route53_record.zone_record.fqdn
    s3_storage_bucket_name   = aws_s3_bucket.recipe_s3_bucket.bucket
    s3_storage_bucket_region = data.aws_region.current.name

  }

}


resource "aws_ecs_task_definition" "api_task_definition" {
  family                   = "${local.prefix}-api-task-definition"
  container_definitions    = data.template_file.api_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.recipe_app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags

}


resource "aws_security_group" "ecs_service_sg" {
  description = "access for the ecs service"
  name        = "${local.prefix}-ecs-service-sg"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_subnet_a.cidr_block,
      aws_subnet.private_subnet_b.cidr_block
    ]
  }

  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    security_groups = [
      aws_security_group.alb_sg.id
    ]
  }

  tags = local.common_tags

}


resource "aws_ecs_service" "api_ecs_service" {
  name             = "${local.prefix}-api-ecs-service"
  cluster          = aws_ecs_cluster.recipe_ecs_cluster.name
  task_definition  = aws_ecs_task_definition.api_task_definition.family
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    subnets = [
      aws_subnet.private_subnet_a.id,
      aws_subnet.private_subnet_b.id
    ]

    security_groups = [aws_security_group.ecs_service_sg.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.recipe_alb_tg.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  depends_on = [aws_lb_listener.recipe_alb_listner_https]

}



data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.recipe_s3_bucket.arn

  }
}


resource "aws_iam_policy" "ecs_s3_access_policy" {
  name        = "${local.prefix}-ecs-s3-access-policy"
  path        = "/"
  description = "allow access to the recipe app s3 bucket"

  policy = data.template_file.ecs_s3_write_policy.rendered

}


resource "aws_iam_role_policy_attachment" "ecs_s3_access_policy_attach" {
  role       = aws_iam_role.recipe_app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access_policy.arn

}
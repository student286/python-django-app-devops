data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_iam_role" "bastion_role" {
  name               = "${local.prefix}-bastion-role"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags

}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion_instance_profile" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion_role.name
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  key_name      = var.bastion_key_name
  subnet_id     = aws_subnet.public_subnet_a.id

  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  iam_instance_profile = aws_iam_instance_profile.bastion_instance_profile.name

  user_data = file("./templates/bastion/user-data.sh")

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}


resource "aws_security_group" "bastion_sg" {
  description = "controll bastion inbound and outbound"
  name        = "${local.prefix}-bastion-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    cidr_blocks = [
      aws_subnet.private_subnet_a.cidr_block,
      aws_subnet.private_subnet_b.cidr_block
    ]
  }

  tags = local.common_tags

}
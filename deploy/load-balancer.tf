resource "aws_lb" "recipe_alb" {
  name               = "${local.prefix}-recpe-alb"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_subnet_a.id,
    aws_subnet.public_subnet_b.id
  ]

  security_groups = [aws_security_group.alb_sg.id]

  tags = local.common_tags

}


resource "aws_lb_target_group" "recipe_alb_tg" {
  name        = "${local.prefix}-recipe-alb-tg"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  port        = 8000

  health_check {
    path = "/admin/login/"
  }
}


resource "aws_lb_listener" "recipe_alb_listner" {
  load_balancer_arn = aws_lb.recipe_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }

  }
}

resource "aws_lb_listener" "recipe_alb_listner_https" {
  load_balancer_arn = aws_lb.recipe_alb.arn
  port              = 443
  protocol          = "HTTPS"

  certificate_arn = aws_acm_certificate_validation.cert_validation.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.recipe_alb_tg.arn

  }
}


resource "aws_security_group" "alb_sg" {
  description = "allow access to applicaton load balancer"
  name        = "${local.prefix}-alb-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags

}

